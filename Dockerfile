FROM       centos:centos7.1.1503
MAINTAINER jiafei <jf200088@gmail.com>
ENV TZ Asia/Shanghai
ENV TERM xterm
ENV ver_nginx 1.7.6
ENV ver_luajit 2.0.3
ENV ver_ngx_devel_kit 0.2.19
ENV ver_ngx_lua_module 0.9.13rc1

ENV LUAJIT_LIB /usr/local/lj2/lib/ 
ENV LUAJIT_INC /usr/local/lj2/include/luajit-2.0/

ADD aliyun-mirror.repo /etc/yum.repos.d/CentOS-Base.repo
ADD aliyun-epel.repo /etc/yum.repos.d/epel.repo


RUN yum clean all && \
    yum makecache

#安装升级相关系统软件
RUN yum install -y curl wget tar bzip2 unzip vim-enhanced passwd sudo yum-utils hostname net-tools rsync man && \
    yum install -y gcc gcc-c++ git make automake cmake patch logrotate python-devel libpng-devel libjpeg-devel pcre pcre-devel && \
    yum install -y --enablerepo=epel pwgen python-pip && \
    yum clean all



#安装nginx
RUN mkdir -p /tmp/ngx_lua_waf
WORKDIR /tmp/ngx_lua_waf
##安装luajit
RUN wget http://luajit.org/download/LuaJIT-$ver_luajit.tar.gz
#ADD LuaJIT-$ver_luajit.tar.gz /tmp/ngx_lua_waf
RUN tar zxvf LuaJIT-$ver_luajit.tar.gz
WORKDIR LuaJIT-$ver_luajit
RUN make && \
    make install PREFIX=/usr/local/lj2
RUN ln -s /usr/local/lj2/lib/libluajit-5.1.so.2 /lib64/

WORKDIR /tmp/ngx_lua_waf

## get ngx devel kit
RUN wget https://github.com/simpl/ngx_devel_kit/archive/v$ver_ngx_devel_kit.zip
RUN unzip v$ver_ngx_devel_kit.zip

## get nginx lua module
RUN wget https://github.com/chaoslawful/lua-nginx-module/archive/v$ver_ngx_lua_module.zip
RUN unzip v$ver_ngx_lua_module.zip

## install nginx
RUN wget http://nginx.org/download/nginx-$ver_nginx.tar.gz
RUN tar -xzvf nginx-$ver_nginx.tar.gz
WORKDIR nginx-$ver_nginx
RUN ./configure --user=www --group=www --prefix=/usr/local/nginx/ --with-http_stub_status_module --with-http_sub_module --with-http_gzip_static_module --without-mail_pop3_module --without-mail_imap_module --without-mail_smtp_module  --add-module=../ngx_devel_kit-$ver_ngx_devel_kit/ --add-module=../lua-nginx-module-$ver_ngx_lua_module/

RUN make -j8 && \
    make install

## install waf
RUN mkdir /usr/local/nginx/conf/waf
WORKDIR /usr/local/nginx/conf/waf
RUN wget https://github.com/loveshell/ngx_lua_waf/archive/master.zip --no-check-certificate
RUN unzip master.zip
RUN mv ngx_lua_waf-master/* /usr/local/nginx/conf/waf

## clear
RUN rm -rf ngx_lua_waf-master
RUN rm -rf /tmp/ngx_lua_waf

RUN groupadd -f www
RUN useradd -g www www

RUN mkdir -p /var/log/nginx
RUN mkdir -p /var/www/html

RUN touch /var/www/html/index.htm
RUN echo "test" > /var/www/html/index.htm

ADD localhost.conf /usr/local/nginx/conf/localhost.conf
ADD nginx.conf /usr/local/nginx/conf/nginx.conf
ADD waf.conf /usr/local/nginx/conf/waf.conf

#安装supervisor
RUN mkdir -p /etc/supervisor.conf.d && \
    mkdir -p /var/log/supervisor

RUN pip install supervisor
ADD supervisor_nginx.conf /etc/supervisor.conf.d/nginx.conf
ADD supervisord.conf /etc/supervisord.conf

EXPOSE 22 80

ENTRYPOINT ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]

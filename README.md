#waf

**用途：**

防止sql注入，本地包含，部分溢出，fuzzing测试，xss,SSRF等web攻击
防止svn/备份之类文件泄漏
防止ApacheBench之类压力测试工具的攻击
屏蔽常见的扫描黑客工具，扫描器
屏蔽异常的网络请求
屏蔽图片附件类目录php执行权限
防止webshell上传


基于nginx和lua的waf应用服务防火墙，lua_ngx_waf模块来至于[https://github.com/loveshell/ngx_lua_waf](https://github.com/loveshell/ngx_lua_waf)

docker image采用centos7作为基础镜像，编译安装了nginx及lua，nginx的启动采用supervisord

**aliyun-epel.repo和aliyun-mirror.repo**是阿里云源的配置文件，不用修改，如果不用，请删除Dockfile中的
ADD aliyun-mirror.repo /etc/yum.repos.d/CentOS-Base.repo
ADD aliyun-epel.repo /etc/yum.repos.d/epel.repo
RUN yum clean all && \
    yum makecache

**install.sh** 是在centos中安装的脚本文件

**localhost.conf**是nginx配置文件，根据自己的网站需求进行相应的修改

**nginx.conf**是nginx的初始化配置文件

**waf.conf**是waf的配置文件千万不要修改或删除

**supervisor_nginx.conf和supervisord.conf**是supervisor的配置文件


**检查是否生效**

异地访问 curl http://xxxx/?id=../etc/passwd

访问不能访问的字样，说明配置生效

注意:默认，本机在白名单不过滤，可自行调整config.lua配置


